unit IndexController;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, Data.DB, superobject,
  BaseController;

type
  TIndexController = class(TBaseController)
  public
    procedure Index();
  end;

implementation

uses
  GroupInterface, GroupService, ContentInterface, ContentService;

{ TIndexController }

var
  group_service: IGroupInterface;
  content_service: IContentInterface;

procedure TIndexController.Index;
var
  json: ISuperObject;
  id: Integer;
  i: Integer;
  content_jo: ISuperObject;
begin
  group_service := TGroupService.Create(view.Db);
  content_service := TContentService.Create(View.Db);
  with View do
  begin

    json := group_service.getList;
    for i := 0 to json.AsArray.Length - 1 do
    begin
      id := json.AsArray[i].i['id'];
      content_jo := content_service.getList(id);
      json.AsArray[i].O['content'] := content_jo;
    end;
    setAttr('user',SessionGet('user'));
    setAttr('groupls', json.AsString);
    ShowHTML('index');
  end;
end;

end.

