unit LoginController;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, Data.DB, superobject,
  BaseController;

type
  TLoginController = class(TBaseController)
  public
    procedure Index;
    procedure ValidateCode;
    procedure check;
    procedure logout;
  end;

implementation

uses
  UsersInterface, UsersService;

{ TLoginController }
var
  user_service: IUsersInterface;

procedure TLoginController.check;
var
  UserName, pwd, code: string;
  wh: ISuperObject;
  user: ISuperObject;
begin
  user_service := TUsersService.Create(view.Db);
  with view do
  begin
    UserName := Input('username');
    pwd := Input('pwd');
    code := Input('ValidateCode');
    if SessionGet('num') = code then
    begin
      wh := SO();
      wh.S['username'] := UserName;
      wh.S['pwd'] := pwd;
      user := user_service.checkuser(wh);
      if user <> nil then
      begin
        SessionSet('user', user.AsString);
        Success();
      end
      else
      begin
        Fail(-1, '�˺��������');
      end;

    end
    else
    begin
      Fail(-1, '��֤�����');
    end;

  end;
end;

procedure TLoginController.Index;
begin
  with view do
  begin
    ShowHTML('login');
  end;
end;

procedure TLoginController.logout;
begin
  view.SessionRemove('user');
  view.Redirect('');
end;

procedure TLoginController.ValidateCode;
var
  num: string;
begin
  Randomize;
  num := inttostr(Random(9)) + inttostr(Random(9)) + inttostr(Random(9)) + inttostr(Random(9));
  View.SessionSet('num', num);
  View.ShowCheckIMG(num, 60, 30);
end;

end.

