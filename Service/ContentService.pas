unit ContentService;

interface

uses
  ContentInterface, superobject, BaseService, uTableMap, System.SysUtils;

type
  TContentService = class(TBaseService, IContentInterface)
  public
    function add(data: ISuperObject): Boolean;
    function edit(data: ISuperObject): Boolean;
    function deletebyid(id: Integer): Boolean;
    function getList(groupid: Integer): ISuperObject;
    function getPage(var count: integer; pagenum: integer; pagesize: Integer; groupid: integer): ISuperObject;
    function getById(id: integer): ISuperObject;
  end;

implementation

{ TContentService }

function TContentService.add(data: ISuperObject): Boolean;
begin

end;

function TContentService.deletebyid(id: Integer): Boolean;
begin

end;

function TContentService.edit(data: ISuperObject): Boolean;
begin

end;

function TContentService.getById(id: integer): ISuperObject;
begin
  Result := Db.FindByKey(tb_content, 'id', id);
end;

function TContentService.getList(groupid: Integer): ISuperObject;
begin
  Result := Db.Find(tb_content, 'and group_id=' + inttostr(groupid));
end;

function TContentService.getPage(var count: integer; pagenum, pagesize: Integer; groupid: integer): ISuperObject;
var
  sql: string;
begin
  if groupid > 0 then
    sql := 'and group_id=' + IntToStr(groupid)
  else
    sql := '';
  Result := Db.FindPage(count, tb_content, sql, 'id desc', pagenum, pagesize);
end;

end.

