unit DiscussInterface;

interface
uses
  superobject;

type
  IDiscussInterface = interface
    function add(data: ISuperObject): Boolean;
    function edit(data: ISuperObject): Boolean;
    function deletebyid(id: Integer): Boolean;
    function getList(content_id: Integer): ISuperObject;
    function getById(id:integer):ISuperObject;
    function getPage(var count: integer; pagenum: integer; pagesize: Integer;content_id: Integer): ISuperObject;
  end;
implementation

end.
