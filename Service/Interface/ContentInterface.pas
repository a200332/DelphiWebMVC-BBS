unit ContentInterface;

interface

uses
  superobject;

type
  IContentInterface = interface
    function add(data: ISuperObject): Boolean;
    function edit(data: ISuperObject): Boolean;
    function deletebyid(id: Integer): Boolean;
    function getList(groupid: Integer): ISuperObject;
    function getById(id:integer):ISuperObject;
    function getPage(var count: integer; pagenum: integer; pagesize: Integer;groupid:integer): ISuperObject;
  end;

implementation

end.

