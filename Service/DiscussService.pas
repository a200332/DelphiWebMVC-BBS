unit DiscussService;

interface

uses
  DiscussInterface, superobject, BaseService, uTableMap, System.SysUtils;

type
  TDiscussService = class(TBaseService, IDiscussInterface)
  public
    function add(data: ISuperObject): Boolean;
    function edit(data: ISuperObject): Boolean;
    function deletebyid(id: Integer): Boolean;
    function getList(content_id: Integer): ISuperObject;
    function getById(id: integer): ISuperObject;
    function getPage(var count: integer; pagenum: integer; pagesize: Integer; content_id: Integer): ISuperObject;
  end;

implementation

{ TDiscussService }

function TDiscussService.add(data: ISuperObject): Boolean;
begin

end;

function TDiscussService.deletebyid(id: Integer): Boolean;
begin

end;

function TDiscussService.edit(data: ISuperObject): Boolean;
begin

end;

function TDiscussService.getById(id: integer): ISuperObject;
begin

end;

function TDiscussService.getList(content_id: Integer): ISuperObject;
begin
  Result := Db.Find(tb_discuss, 'and content_id=' + IntToStr(content_id));
end;

function TDiscussService.getPage(var count: integer; pagenum, pagesize, content_id: Integer): ISuperObject;
begin
  Result := Db.FindPage(count, tb_content, 'and content_id=' + IntToStr(content_id), 'id desc', pagenum, pagesize);
end;

end.

